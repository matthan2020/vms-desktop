﻿namespace VMS_AZVISION
{
    partial class FrmTrangChu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTrangChu));
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.btnCauHinhHeThong = new System.Windows.Forms.Button();
            this.btnNguoiDung = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.btnCauHinh = new System.Windows.Forms.Button();
            this.btnThietBi = new System.Windows.Forms.Button();
            this.btnDangNhap = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.btnPhatLai = new System.Windows.Forms.Button();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.btnChamCong = new System.Windows.Forms.Button();
            this.btnSuKien = new System.Windows.Forms.Button();
            this.btnXemTrucTiep = new System.Windows.Forms.Button();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblDongHo = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.picUser = new System.Windows.Forms.PictureBox();
            this.picConfig = new System.Windows.Forms.PictureBox();
            this.picMinimize = new System.Windows.Forms.PictureBox();
            this.picMaxRestore = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.picClose = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel4.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picConfig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMinimize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMaxRestore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picClose)).BeginInit();
            this.SuspendLayout();
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel13);
            this.panel4.Controls.Add(this.panel7);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Controls.Add(this.panel12);
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Controls.Add(this.panel11);
            this.panel4.Controls.Add(this.panel10);
            this.panel4.Controls.Add(this.panel9);
            this.panel4.Controls.Add(this.panel1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1005, 600);
            this.panel4.TabIndex = 1;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(52)))), ((int)(((byte)(58)))));
            this.panel13.Controls.Add(this.btnCauHinhHeThong);
            this.panel13.Controls.Add(this.btnNguoiDung);
            this.panel13.Controls.Add(this.label8);
            this.panel13.Controls.Add(this.btnCauHinh);
            this.panel13.Controls.Add(this.btnThietBi);
            this.panel13.Controls.Add(this.btnDangNhap);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel13.Location = new System.Drawing.Point(32, 531);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(941, 166);
            this.panel13.TabIndex = 13;
            // 
            // btnCauHinhHeThong
            // 
            this.btnCauHinhHeThong.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.btnCauHinhHeThong.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.btnCauHinhHeThong.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCauHinhHeThong.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCauHinhHeThong.ForeColor = System.Drawing.Color.White;
            this.btnCauHinhHeThong.Image = global::VMS_AZVISION.Properties.Resources.Group_309;
            this.btnCauHinhHeThong.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCauHinhHeThong.Location = new System.Drawing.Point(654, 37);
            this.btnCauHinhHeThong.Name = "btnCauHinhHeThong";
            this.btnCauHinhHeThong.Size = new System.Drawing.Size(139, 107);
            this.btnCauHinhHeThong.TabIndex = 8;
            this.btnCauHinhHeThong.Text = "Cấu hình hệ thống";
            this.btnCauHinhHeThong.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnCauHinhHeThong.UseVisualStyleBackColor = false;
            this.btnCauHinhHeThong.Click += new System.EventHandler(this.btnCauHinhHeThong_Click);
            // 
            // btnNguoiDung
            // 
            this.btnNguoiDung.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.btnNguoiDung.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.btnNguoiDung.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNguoiDung.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNguoiDung.ForeColor = System.Drawing.Color.White;
            this.btnNguoiDung.Image = global::VMS_AZVISION.Properties.Resources.dang_nhap;
            this.btnNguoiDung.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnNguoiDung.Location = new System.Drawing.Point(499, 37);
            this.btnNguoiDung.Name = "btnNguoiDung";
            this.btnNguoiDung.Size = new System.Drawing.Size(139, 107);
            this.btnNguoiDung.TabIndex = 7;
            this.btnNguoiDung.Text = "Quản lý tài khoản";
            this.btnNguoiDung.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnNguoiDung.UseVisualStyleBackColor = false;
            this.btnNguoiDung.Click += new System.EventHandler(this.btnNguoiDung_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(34, 3);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 21);
            this.label8.TabIndex = 6;
            this.label8.Text = "Quản lý";
            // 
            // btnCauHinh
            // 
            this.btnCauHinh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.btnCauHinh.Enabled = false;
            this.btnCauHinh.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.btnCauHinh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCauHinh.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCauHinh.ForeColor = System.Drawing.Color.White;
            this.btnCauHinh.Image = global::VMS_AZVISION.Properties.Resources.Event_setting;
            this.btnCauHinh.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCauHinh.Location = new System.Drawing.Point(345, 37);
            this.btnCauHinh.Name = "btnCauHinh";
            this.btnCauHinh.Size = new System.Drawing.Size(139, 107);
            this.btnCauHinh.TabIndex = 5;
            this.btnCauHinh.Text = "Cấu hình sự kiện";
            this.btnCauHinh.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnCauHinh.UseVisualStyleBackColor = false;
            // 
            // btnThietBi
            // 
            this.btnThietBi.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.btnThietBi.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.btnThietBi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnThietBi.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThietBi.ForeColor = System.Drawing.Color.White;
            this.btnThietBi.Image = global::VMS_AZVISION.Properties.Resources.Thiet_bi;
            this.btnThietBi.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnThietBi.Location = new System.Drawing.Point(191, 37);
            this.btnThietBi.Name = "btnThietBi";
            this.btnThietBi.Size = new System.Drawing.Size(139, 107);
            this.btnThietBi.TabIndex = 4;
            this.btnThietBi.Text = "Thiết bị";
            this.btnThietBi.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnThietBi.UseVisualStyleBackColor = false;
            this.btnThietBi.Click += new System.EventHandler(this.btnThietBi_Click);
            // 
            // btnDangNhap
            // 
            this.btnDangNhap.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.btnDangNhap.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.btnDangNhap.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDangNhap.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDangNhap.ForeColor = System.Drawing.Color.White;
            this.btnDangNhap.Image = global::VMS_AZVISION.Properties.Resources.Login;
            this.btnDangNhap.Location = new System.Drawing.Point(38, 37);
            this.btnDangNhap.Name = "btnDangNhap";
            this.btnDangNhap.Size = new System.Drawing.Size(139, 107);
            this.btnDangNhap.TabIndex = 3;
            this.btnDangNhap.Text = "Đăng nhập lại";
            this.btnDangNhap.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnDangNhap.UseVisualStyleBackColor = false;
            // 
            // panel7
            // 
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(32, 476);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(941, 55);
            this.panel7.TabIndex = 12;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(52)))), ((int)(((byte)(58)))));
            this.panel6.Controls.Add(this.label7);
            this.panel6.Controls.Add(this.btnPhatLai);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(32, 310);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(941, 166);
            this.panel6.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(34, 7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 21);
            this.label7.TabIndex = 6;
            this.label7.Text = "Tìm";
            // 
            // btnPhatLai
            // 
            this.btnPhatLai.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.btnPhatLai.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.btnPhatLai.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPhatLai.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPhatLai.ForeColor = System.Drawing.Color.White;
            this.btnPhatLai.Image = global::VMS_AZVISION.Properties.Resources.Replay;
            this.btnPhatLai.Location = new System.Drawing.Point(33, 42);
            this.btnPhatLai.Name = "btnPhatLai";
            this.btnPhatLai.Size = new System.Drawing.Size(139, 107);
            this.btnPhatLai.TabIndex = 3;
            this.btnPhatLai.Text = "Phát lại";
            this.btnPhatLai.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnPhatLai.UseVisualStyleBackColor = false;
            this.btnPhatLai.Click += new System.EventHandler(this.btnPhatLai_Click);
            // 
            // panel12
            // 
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(32, 255);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(941, 55);
            this.panel12.TabIndex = 10;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(52)))), ((int)(((byte)(58)))));
            this.panel5.Controls.Add(this.label6);
            this.panel5.Controls.Add(this.btnChamCong);
            this.panel5.Controls.Add(this.btnSuKien);
            this.panel5.Controls.Add(this.btnXemTrucTiep);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(32, 89);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(941, 166);
            this.panel5.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(34, 13);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 21);
            this.label6.TabIndex = 3;
            this.label6.Text = "Hoạt động";
            // 
            // btnChamCong
            // 
            this.btnChamCong.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.btnChamCong.Enabled = false;
            this.btnChamCong.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.btnChamCong.FlatAppearance.BorderSize = 0;
            this.btnChamCong.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChamCong.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChamCong.ForeColor = System.Drawing.Color.White;
            this.btnChamCong.Image = global::VMS_AZVISION.Properties.Resources.cham_cong;
            this.btnChamCong.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnChamCong.Location = new System.Drawing.Point(340, 42);
            this.btnChamCong.Name = "btnChamCong";
            this.btnChamCong.Size = new System.Drawing.Size(139, 106);
            this.btnChamCong.TabIndex = 2;
            this.btnChamCong.Text = "Chấm công";
            this.btnChamCong.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnChamCong.UseVisualStyleBackColor = false;
            // 
            // btnSuKien
            // 
            this.btnSuKien.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.btnSuKien.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.btnSuKien.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSuKien.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuKien.ForeColor = System.Drawing.Color.White;
            this.btnSuKien.Image = global::VMS_AZVISION.Properties.Resources.Event;
            this.btnSuKien.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSuKien.Location = new System.Drawing.Point(186, 42);
            this.btnSuKien.Name = "btnSuKien";
            this.btnSuKien.Size = new System.Drawing.Size(139, 106);
            this.btnSuKien.TabIndex = 1;
            this.btnSuKien.Text = "Sự kiện";
            this.btnSuKien.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnSuKien.UseVisualStyleBackColor = false;
            this.btnSuKien.Click += new System.EventHandler(this.btnSuKien_Click);
            // 
            // btnXemTrucTiep
            // 
            this.btnXemTrucTiep.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.btnXemTrucTiep.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.btnXemTrucTiep.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnXemTrucTiep.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemTrucTiep.ForeColor = System.Drawing.Color.White;
            this.btnXemTrucTiep.Image = global::VMS_AZVISION.Properties.Resources.Live;
            this.btnXemTrucTiep.Location = new System.Drawing.Point(33, 41);
            this.btnXemTrucTiep.Name = "btnXemTrucTiep";
            this.btnXemTrucTiep.Size = new System.Drawing.Size(139, 107);
            this.btnXemTrucTiep.TabIndex = 0;
            this.btnXemTrucTiep.Text = "Xem trực tiếp";
            this.btnXemTrucTiep.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnXemTrucTiep.UseVisualStyleBackColor = false;
            this.btnXemTrucTiep.Click += new System.EventHandler(this.btnXemTrucTiep_Click);
            // 
            // panel11
            // 
            this.panel11.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel11.Location = new System.Drawing.Point(973, 89);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(32, 511);
            this.panel11.TabIndex = 8;
            // 
            // panel10
            // 
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(0, 89);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(32, 511);
            this.panel10.TabIndex = 7;
            // 
            // panel9
            // 
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 61);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1005, 28);
            this.panel9.TabIndex = 6;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(52)))), ((int)(((byte)(58)))));
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1005, 61);
            this.panel1.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(52)))), ((int)(((byte)(58)))));
            this.label5.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(871, 12);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 43);
            this.label5.TabIndex = 8;
            this.label5.Text = "+";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.pictureBox1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(234, 61);
            this.panel3.TabIndex = 2;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::VMS_AZVISION.Properties.Resources.LOGO_ngang_Chu_trang;
            this.pictureBox1.Location = new System.Drawing.Point(4, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(230, 55);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(52)))), ((int)(((byte)(58)))));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(713, 12);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(150, 43);
            this.label4.TabIndex = 7;
            this.label4.Text = "Xem trực tiếp";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lblDongHo);
            this.panel2.Controls.Add(this.panel8);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(789, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(216, 61);
            this.panel2.TabIndex = 1;
            // 
            // lblDongHo
            // 
            this.lblDongHo.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblDongHo.ForeColor = System.Drawing.Color.White;
            this.lblDongHo.Location = new System.Drawing.Point(129, 33);
            this.lblDongHo.Name = "lblDongHo";
            this.lblDongHo.Size = new System.Drawing.Size(87, 28);
            this.lblDongHo.TabIndex = 1;
            this.lblDongHo.Text = "19:19:19";
            this.lblDongHo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.picUser);
            this.panel8.Controls.Add(this.picConfig);
            this.panel8.Controls.Add(this.picMinimize);
            this.panel8.Controls.Add(this.picMaxRestore);
            this.panel8.Controls.Add(this.pictureBox3);
            this.panel8.Controls.Add(this.picClose);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(216, 33);
            this.panel8.TabIndex = 0;
            // 
            // picUser
            // 
            this.picUser.Dock = System.Windows.Forms.DockStyle.Right;
            this.picUser.Image = global::VMS_AZVISION.Properties.Resources.Group_612;
            this.picUser.Location = new System.Drawing.Point(30, 0);
            this.picUser.Name = "picUser";
            this.picUser.Size = new System.Drawing.Size(31, 33);
            this.picUser.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picUser.TabIndex = 5;
            this.picUser.TabStop = false;
            this.picUser.Click += new System.EventHandler(this.picUser_Click);
            // 
            // picConfig
            // 
            this.picConfig.Dock = System.Windows.Forms.DockStyle.Right;
            this.picConfig.Image = global::VMS_AZVISION.Properties.Resources.Path_117;
            this.picConfig.Location = new System.Drawing.Point(61, 0);
            this.picConfig.Name = "picConfig";
            this.picConfig.Size = new System.Drawing.Size(31, 33);
            this.picConfig.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picConfig.TabIndex = 4;
            this.picConfig.TabStop = false;
            this.picConfig.Click += new System.EventHandler(this.picConfig_Click);
            // 
            // picMinimize
            // 
            this.picMinimize.Dock = System.Windows.Forms.DockStyle.Right;
            this.picMinimize.Image = global::VMS_AZVISION.Properties.Resources.Minimize;
            this.picMinimize.Location = new System.Drawing.Point(92, 0);
            this.picMinimize.Name = "picMinimize";
            this.picMinimize.Size = new System.Drawing.Size(31, 33);
            this.picMinimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picMinimize.TabIndex = 3;
            this.picMinimize.TabStop = false;
            // 
            // picMaxRestore
            // 
            this.picMaxRestore.Dock = System.Windows.Forms.DockStyle.Right;
            this.picMaxRestore.Image = global::VMS_AZVISION.Properties.Resources.Full;
            this.picMaxRestore.Location = new System.Drawing.Point(123, 0);
            this.picMaxRestore.Name = "picMaxRestore";
            this.picMaxRestore.Size = new System.Drawing.Size(31, 33);
            this.picMaxRestore.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picMaxRestore.TabIndex = 2;
            this.picMaxRestore.TabStop = false;
            this.picMaxRestore.Click += new System.EventHandler(this.picMaxRestore_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox3.Location = new System.Drawing.Point(154, 0);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(31, 33);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox3.TabIndex = 1;
            this.pictureBox3.TabStop = false;
            // 
            // picClose
            // 
            this.picClose.Dock = System.Windows.Forms.DockStyle.Right;
            this.picClose.Image = global::VMS_AZVISION.Properties.Resources.Close;
            this.picClose.Location = new System.Drawing.Point(185, 0);
            this.picClose.Name = "picClose";
            this.picClose.Size = new System.Drawing.Size(31, 33);
            this.picClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picClose.TabIndex = 0;
            this.picClose.TabStop = false;
            this.picClose.Click += new System.EventHandler(this.picClose_Click);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(52)))), ((int)(((byte)(58)))));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(556, 12);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(150, 43);
            this.label3.TabIndex = 6;
            this.label3.Text = "Người dùng";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(52)))), ((int)(((byte)(58)))));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(183)))), ((int)(((byte)(255)))));
            this.label2.Location = new System.Drawing.Point(241, 12);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(150, 43);
            this.label2.TabIndex = 4;
            this.label2.Text = "Ứng dụng";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(52)))), ((int)(((byte)(58)))));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(398, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(150, 43);
            this.label1.TabIndex = 5;
            this.label1.Text = "Thiết bị";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // FrmTrangChu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.ClientSize = new System.Drawing.Size(1005, 600);
            this.Controls.Add(this.panel4);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FrmTrangChu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Trang chủ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel4.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picConfig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMinimize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMaxRestore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picClose)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblDongHo;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.PictureBox picUser;
        private System.Windows.Forms.PictureBox picConfig;
        private System.Windows.Forms.PictureBox picMinimize;
        private System.Windows.Forms.PictureBox picMaxRestore;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox picClose;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Button btnCauHinhHeThong;
        private System.Windows.Forms.Button btnNguoiDung;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnCauHinh;
        private System.Windows.Forms.Button btnThietBi;
        private System.Windows.Forms.Button btnDangNhap;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnPhatLai;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnChamCong;
        private System.Windows.Forms.Button btnSuKien;
        private System.Windows.Forms.Button btnXemTrucTiep;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Timer timer1;
    }
}