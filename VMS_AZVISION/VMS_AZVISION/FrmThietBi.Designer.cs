﻿namespace VMS_AZVISION
{
    partial class FrmThietBi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblDongHo = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.picUser = new System.Windows.Forms.PictureBox();
            this.picConfig = new System.Windows.Forms.PictureBox();
            this.picMinimize = new System.Windows.Forms.PictureBox();
            this.picMaxRestore = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.picClose = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lblThietBiKhac = new System.Windows.Forms.Label();
            this.lblThietBiOnline = new System.Windows.Forms.Label();
            this.lblTatCaThietBi = new System.Windows.Forms.Label();
            this.btnTrangThaiThietBi = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnNangCap = new System.Windows.Forms.Button();
            this.btnMaQR = new System.Windows.Forms.Button();
            this.btnCaiDatTuXa = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnThem = new System.Windows.Forms.Button();
            this.grdThietBi = new System.Windows.Forms.DataGridView();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.colStt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDeviceId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLoaiTB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMaThietBi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBaoMat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCong = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTrangThai = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colVanHanh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColXoa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picConfig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMinimize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMaxRestore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picClose)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdThietBi)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1154, 52);
            this.panel2.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.label5.Dock = System.Windows.Forms.DockStyle.Left;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Image = global::VMS_AZVISION.Properties.Resources.Group_620;
            this.label5.Location = new System.Drawing.Point(830, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 52);
            this.label5.TabIndex = 14;
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.label4.Dock = System.Windows.Forms.DockStyle.Left;
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(680, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(150, 52);
            this.label4.TabIndex = 13;
            this.label4.Text = "Xem trực tiếp";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.label3.Dock = System.Windows.Forms.DockStyle.Left;
            this.label3.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(530, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(150, 52);
            this.label3.TabIndex = 12;
            this.label3.Text = "Người dùng";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(183)))), ((int)(((byte)(255)))));
            this.label1.Location = new System.Drawing.Point(380, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(150, 52);
            this.label1.TabIndex = 11;
            this.label1.Text = "Thiết bị";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.label2.Dock = System.Windows.Forms.DockStyle.Left;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(230, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(150, 52);
            this.label2.TabIndex = 10;
            this.label2.Text = "Ứng dụng";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox1.Image = global::VMS_AZVISION.Properties.Resources.LOGO_ngang_Chu_trang;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(230, 52);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.panel4.Controls.Add(this.lblDongHo);
            this.panel4.Controls.Add(this.panel8);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(938, 0);
            this.panel4.Margin = new System.Windows.Forms.Padding(4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(216, 52);
            this.panel4.TabIndex = 1;
            // 
            // lblDongHo
            // 
            this.lblDongHo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.lblDongHo.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblDongHo.ForeColor = System.Drawing.Color.White;
            this.lblDongHo.Location = new System.Drawing.Point(129, 33);
            this.lblDongHo.Name = "lblDongHo";
            this.lblDongHo.Size = new System.Drawing.Size(87, 19);
            this.lblDongHo.TabIndex = 1;
            this.lblDongHo.Text = "19:19:19";
            this.lblDongHo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.picUser);
            this.panel8.Controls.Add(this.picConfig);
            this.panel8.Controls.Add(this.picMinimize);
            this.panel8.Controls.Add(this.picMaxRestore);
            this.panel8.Controls.Add(this.pictureBox3);
            this.panel8.Controls.Add(this.picClose);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(216, 33);
            this.panel8.TabIndex = 0;
            // 
            // picUser
            // 
            this.picUser.Dock = System.Windows.Forms.DockStyle.Right;
            this.picUser.Image = global::VMS_AZVISION.Properties.Resources.Group_612;
            this.picUser.Location = new System.Drawing.Point(30, 0);
            this.picUser.Name = "picUser";
            this.picUser.Size = new System.Drawing.Size(31, 33);
            this.picUser.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picUser.TabIndex = 5;
            this.picUser.TabStop = false;
            this.picUser.Click += new System.EventHandler(this.picUser_Click);
            // 
            // picConfig
            // 
            this.picConfig.Dock = System.Windows.Forms.DockStyle.Right;
            this.picConfig.Image = global::VMS_AZVISION.Properties.Resources.Path_117;
            this.picConfig.Location = new System.Drawing.Point(61, 0);
            this.picConfig.Name = "picConfig";
            this.picConfig.Size = new System.Drawing.Size(31, 33);
            this.picConfig.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picConfig.TabIndex = 4;
            this.picConfig.TabStop = false;
            this.picConfig.Click += new System.EventHandler(this.picConfig_Click);
            // 
            // picMinimize
            // 
            this.picMinimize.Dock = System.Windows.Forms.DockStyle.Right;
            this.picMinimize.Image = global::VMS_AZVISION.Properties.Resources.Minimize;
            this.picMinimize.Location = new System.Drawing.Point(92, 0);
            this.picMinimize.Name = "picMinimize";
            this.picMinimize.Size = new System.Drawing.Size(31, 33);
            this.picMinimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picMinimize.TabIndex = 3;
            this.picMinimize.TabStop = false;
            // 
            // picMaxRestore
            // 
            this.picMaxRestore.Dock = System.Windows.Forms.DockStyle.Right;
            this.picMaxRestore.Image = global::VMS_AZVISION.Properties.Resources.Full;
            this.picMaxRestore.Location = new System.Drawing.Point(123, 0);
            this.picMaxRestore.Name = "picMaxRestore";
            this.picMaxRestore.Size = new System.Drawing.Size(31, 33);
            this.picMaxRestore.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picMaxRestore.TabIndex = 2;
            this.picMaxRestore.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox3.Location = new System.Drawing.Point(154, 0);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(31, 33);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox3.TabIndex = 1;
            this.pictureBox3.TabStop = false;
            // 
            // picClose
            // 
            this.picClose.Dock = System.Windows.Forms.DockStyle.Right;
            this.picClose.Image = global::VMS_AZVISION.Properties.Resources.Close;
            this.picClose.Location = new System.Drawing.Point(185, 0);
            this.picClose.Name = "picClose";
            this.picClose.Size = new System.Drawing.Size(31, 33);
            this.picClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picClose.TabIndex = 0;
            this.picClose.TabStop = false;
            this.picClose.Click += new System.EventHandler(this.picClose_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(183)))), ((int)(((byte)(255)))));
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 52);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1154, 2);
            this.panel1.TabIndex = 8;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.panel5.Controls.Add(this.lblThietBiKhac);
            this.panel5.Controls.Add(this.lblThietBiOnline);
            this.panel5.Controls.Add(this.lblTatCaThietBi);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 54);
            this.panel5.Margin = new System.Windows.Forms.Padding(4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1154, 58);
            this.panel5.TabIndex = 9;
            // 
            // lblThietBiKhac
            // 
            this.lblThietBiKhac.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.lblThietBiKhac.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblThietBiKhac.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThietBiKhac.ForeColor = System.Drawing.Color.White;
            this.lblThietBiKhac.Location = new System.Drawing.Point(472, 0);
            this.lblThietBiKhac.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblThietBiKhac.Name = "lblThietBiKhac";
            this.lblThietBiKhac.Size = new System.Drawing.Size(242, 58);
            this.lblThietBiKhac.TabIndex = 2;
            this.lblThietBiKhac.Text = "Thiết bị khác";
            this.lblThietBiKhac.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblThietBiOnline
            // 
            this.lblThietBiOnline.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.lblThietBiOnline.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblThietBiOnline.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThietBiOnline.ForeColor = System.Drawing.Color.White;
            this.lblThietBiOnline.Location = new System.Drawing.Point(230, 0);
            this.lblThietBiOnline.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblThietBiOnline.Name = "lblThietBiOnline";
            this.lblThietBiOnline.Size = new System.Drawing.Size(242, 58);
            this.lblThietBiOnline.TabIndex = 1;
            this.lblThietBiOnline.Text = "Thiết bị online";
            this.lblThietBiOnline.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTatCaThietBi
            // 
            this.lblTatCaThietBi.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.lblTatCaThietBi.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblTatCaThietBi.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTatCaThietBi.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(183)))), ((int)(((byte)(255)))));
            this.lblTatCaThietBi.Location = new System.Drawing.Point(0, 0);
            this.lblTatCaThietBi.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTatCaThietBi.Name = "lblTatCaThietBi";
            this.lblTatCaThietBi.Size = new System.Drawing.Size(230, 58);
            this.lblTatCaThietBi.TabIndex = 0;
            this.lblTatCaThietBi.Text = "Tất cả Thiết bị";
            this.lblTatCaThietBi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnTrangThaiThietBi
            // 
            this.btnTrangThaiThietBi.AllowDrop = true;
            this.btnTrangThaiThietBi.AutoSize = true;
            this.btnTrangThaiThietBi.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnTrangThaiThietBi.FlatAppearance.BorderSize = 0;
            this.btnTrangThaiThietBi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTrangThaiThietBi.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrangThaiThietBi.ForeColor = System.Drawing.Color.White;
            this.btnTrangThaiThietBi.Location = new System.Drawing.Point(366, 0);
            this.btnTrangThaiThietBi.Margin = new System.Windows.Forms.Padding(4);
            this.btnTrangThaiThietBi.Name = "btnTrangThaiThietBi";
            this.btnTrangThaiThietBi.Size = new System.Drawing.Size(164, 58);
            this.btnTrangThaiThietBi.TabIndex = 6;
            this.btnTrangThaiThietBi.Text = "Trạng thái thiết bị";
            this.btnTrangThaiThietBi.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(36)))), ((int)(((byte)(36)))));
            this.panel6.Controls.Add(this.btnNangCap);
            this.panel6.Controls.Add(this.btnTrangThaiThietBi);
            this.panel6.Controls.Add(this.btnMaQR);
            this.panel6.Controls.Add(this.btnCaiDatTuXa);
            this.panel6.Controls.Add(this.panel7);
            this.panel6.Controls.Add(this.btnThem);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 112);
            this.panel6.Margin = new System.Windows.Forms.Padding(4);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1154, 58);
            this.panel6.TabIndex = 11;
            // 
            // btnNangCap
            // 
            this.btnNangCap.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnNangCap.FlatAppearance.BorderSize = 0;
            this.btnNangCap.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNangCap.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNangCap.ForeColor = System.Drawing.Color.White;
            this.btnNangCap.Location = new System.Drawing.Point(530, 0);
            this.btnNangCap.Margin = new System.Windows.Forms.Padding(4);
            this.btnNangCap.Name = "btnNangCap";
            this.btnNangCap.Size = new System.Drawing.Size(122, 58);
            this.btnNangCap.TabIndex = 7;
            this.btnNangCap.Text = "Nâng cấp";
            this.btnNangCap.UseVisualStyleBackColor = true;
            // 
            // btnMaQR
            // 
            this.btnMaQR.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnMaQR.FlatAppearance.BorderSize = 0;
            this.btnMaQR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMaQR.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMaQR.ForeColor = System.Drawing.Color.White;
            this.btnMaQR.Location = new System.Drawing.Point(244, 0);
            this.btnMaQR.Margin = new System.Windows.Forms.Padding(4);
            this.btnMaQR.Name = "btnMaQR";
            this.btnMaQR.Size = new System.Drawing.Size(122, 58);
            this.btnMaQR.TabIndex = 5;
            this.btnMaQR.Text = "Mã QR";
            this.btnMaQR.UseVisualStyleBackColor = true;
            // 
            // btnCaiDatTuXa
            // 
            this.btnCaiDatTuXa.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnCaiDatTuXa.FlatAppearance.BorderSize = 0;
            this.btnCaiDatTuXa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCaiDatTuXa.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCaiDatTuXa.ForeColor = System.Drawing.Color.White;
            this.btnCaiDatTuXa.Location = new System.Drawing.Point(122, 0);
            this.btnCaiDatTuXa.Margin = new System.Windows.Forms.Padding(4);
            this.btnCaiDatTuXa.Name = "btnCaiDatTuXa";
            this.btnCaiDatTuXa.Size = new System.Drawing.Size(122, 58);
            this.btnCaiDatTuXa.TabIndex = 4;
            this.btnCaiDatTuXa.Text = "Cài đặt từ xa";
            this.btnCaiDatTuXa.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.textBox1);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel7.Location = new System.Drawing.Point(880, 0);
            this.panel7.Margin = new System.Windows.Forms.Padding(4);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(274, 58);
            this.panel7.TabIndex = 2;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(36)))), ((int)(((byte)(36)))));
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.ForeColor = System.Drawing.Color.White;
            this.textBox1.Location = new System.Drawing.Point(18, 14);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(243, 27);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "Tìm kiếm";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnThem
            // 
            this.btnThem.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnThem.FlatAppearance.BorderSize = 0;
            this.btnThem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnThem.Image = global::VMS_AZVISION.Properties.Resources.Group_170;
            this.btnThem.Location = new System.Drawing.Point(0, 0);
            this.btnThem.Margin = new System.Windows.Forms.Padding(4);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(122, 58);
            this.btnThem.TabIndex = 0;
            this.btnThem.UseVisualStyleBackColor = true;
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // grdThietBi
            // 
            this.grdThietBi.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grdThietBi.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.grdThietBi.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.grdThietBi.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdThietBi.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.grdThietBi.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdThietBi.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.grdThietBi.ColumnHeadersHeight = 35;
            this.grdThietBi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.grdThietBi.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colStt,
            this.ColDeviceId,
            this.colTen,
            this.colIP,
            this.colLoaiTB,
            this.colMaThietBi,
            this.colBaoMat,
            this.colCong,
            this.colTrangThai,
            this.colVanHanh,
            this.ColXoa});
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(43)))));
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(23)))), ((int)(((byte)(23)))));
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(194)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdThietBi.DefaultCellStyle = dataGridViewCellStyle26;
            this.grdThietBi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdThietBi.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(97)))), ((int)(((byte)(97)))));
            this.grdThietBi.Location = new System.Drawing.Point(0, 170);
            this.grdThietBi.Margin = new System.Windows.Forms.Padding(4);
            this.grdThietBi.MultiSelect = false;
            this.grdThietBi.Name = "grdThietBi";
            this.grdThietBi.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle27.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(194)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdThietBi.RowHeadersDefaultCellStyle = dataGridViewCellStyle27;
            this.grdThietBi.RowHeadersVisible = false;
            dataGridViewCellStyle28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(43)))));
            dataGridViewCellStyle28.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(23)))), ((int)(((byte)(23)))));
            dataGridViewCellStyle28.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(194)))), ((int)(((byte)(255)))));
            this.grdThietBi.RowsDefaultCellStyle = dataGridViewCellStyle28;
            this.grdThietBi.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdThietBi.Size = new System.Drawing.Size(1154, 500);
            this.grdThietBi.TabIndex = 12;
            this.grdThietBi.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdThietBi_CellContentClick);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // colStt
            // 
            this.colStt.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colStt.DataPropertyName = "STT";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.Black;
            this.colStt.DefaultCellStyle = dataGridViewCellStyle16;
            this.colStt.HeaderText = "STT";
            this.colStt.Name = "colStt";
            this.colStt.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colStt.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colStt.Width = 58;
            // 
            // ColDeviceId
            // 
            this.ColDeviceId.DataPropertyName = "DeviceId";
            this.ColDeviceId.HeaderText = "DeviceId";
            this.ColDeviceId.Name = "ColDeviceId";
            this.ColDeviceId.Visible = false;
            this.ColDeviceId.Width = 82;
            // 
            // colTen
            // 
            this.colTen.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colTen.DataPropertyName = "DeviceName";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.colTen.DefaultCellStyle = dataGridViewCellStyle17;
            this.colTen.HeaderText = "Tên";
            this.colTen.Name = "colTen";
            this.colTen.ReadOnly = true;
            this.colTen.Width = 150;
            // 
            // colIP
            // 
            this.colIP.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colIP.DataPropertyName = "IpAddress";
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.colIP.DefaultCellStyle = dataGridViewCellStyle18;
            this.colIP.HeaderText = "IP/Tên miền";
            this.colIP.Name = "colIP";
            this.colIP.ReadOnly = true;
            this.colIP.Width = 115;
            // 
            // colLoaiTB
            // 
            this.colLoaiTB.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colLoaiTB.DataPropertyName = "DeviceType";
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.colLoaiTB.DefaultCellStyle = dataGridViewCellStyle19;
            this.colLoaiTB.HeaderText = "Loại thiết bị";
            this.colLoaiTB.Name = "colLoaiTB";
            this.colLoaiTB.ReadOnly = true;
            this.colLoaiTB.Width = 114;
            // 
            // colMaThietBi
            // 
            this.colMaThietBi.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colMaThietBi.DataPropertyName = "DeviceCode";
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.colMaThietBi.DefaultCellStyle = dataGridViewCellStyle20;
            this.colMaThietBi.HeaderText = "Mã thiết bị (Số seri)";
            this.colMaThietBi.Name = "colMaThietBi";
            this.colMaThietBi.ReadOnly = true;
            // 
            // colBaoMat
            // 
            this.colBaoMat.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colBaoMat.DataPropertyName = "SecurityLevel";
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.colBaoMat.DefaultCellStyle = dataGridViewCellStyle21;
            this.colBaoMat.HeaderText = "Bảo mật";
            this.colBaoMat.Name = "colBaoMat";
            this.colBaoMat.ReadOnly = true;
            this.colBaoMat.Width = 90;
            // 
            // colCong
            // 
            this.colCong.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colCong.DataPropertyName = "PortNo";
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colCong.DefaultCellStyle = dataGridViewCellStyle22;
            this.colCong.HeaderText = "Cổng";
            this.colCong.Name = "colCong";
            this.colCong.ReadOnly = true;
            this.colCong.Width = 70;
            // 
            // colTrangThai
            // 
            this.colTrangThai.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colTrangThai.DataPropertyName = "Status";
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.colTrangThai.DefaultCellStyle = dataGridViewCellStyle23;
            this.colTrangThai.HeaderText = "Trạng thái";
            this.colTrangThai.Name = "colTrangThai";
            this.colTrangThai.ReadOnly = true;
            this.colTrangThai.Width = 103;
            // 
            // colVanHanh
            // 
            this.colVanHanh.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colVanHanh.DataPropertyName = "DieuChinh";
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.colVanHanh.DefaultCellStyle = dataGridViewCellStyle24;
            this.colVanHanh.HeaderText = "Vận hành 1";
            this.colVanHanh.Name = "colVanHanh";
            this.colVanHanh.ReadOnly = true;
            this.colVanHanh.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colVanHanh.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ColXoa
            // 
            this.ColXoa.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ColXoa.DataPropertyName = "Xoa";
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ColXoa.DefaultCellStyle = dataGridViewCellStyle25;
            this.ColXoa.HeaderText = "Vận hành 2";
            this.ColXoa.Name = "ColXoa";
            this.ColXoa.ReadOnly = true;
            this.ColXoa.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // FrmThietBi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.ClientSize = new System.Drawing.Size(1154, 670);
            this.Controls.Add(this.grdThietBi);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmThietBi";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmThietBi";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picConfig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMinimize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMaxRestore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picClose)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdThietBi)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lblDongHo;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.PictureBox picUser;
        private System.Windows.Forms.PictureBox picConfig;
        private System.Windows.Forms.PictureBox picMinimize;
        private System.Windows.Forms.PictureBox picMaxRestore;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox picClose;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lblThietBiKhac;
        private System.Windows.Forms.Label lblThietBiOnline;
        private System.Windows.Forms.Label lblTatCaThietBi;
        private System.Windows.Forms.Button btnTrangThaiThietBi;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btnNangCap;
        private System.Windows.Forms.Button btnMaQR;
        private System.Windows.Forms.Button btnCaiDatTuXa;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnThem;
        private System.Windows.Forms.DataGridView grdThietBi;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStt;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDeviceId;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTen;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIP;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLoaiTB;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMaThietBi;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBaoMat;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCong;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTrangThai;
        private System.Windows.Forms.DataGridViewTextBoxColumn colVanHanh;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColXoa;
    }
}