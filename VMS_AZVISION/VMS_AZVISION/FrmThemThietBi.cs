﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Data.SqlClient;
using VMS_AZVISION.Model;
using System.Text.RegularExpressions;

namespace VMS_AZVISION
{
    public partial class FrmThemThietBi : Form
    {
        Thread thread;
        private int idDeviceType;
        private string deviceTypeName = "";

        private string tenThietBi = ""; // IpAddress
        private string diaChiIP = "";
        private string congKetNoi = ""; // PortNo
        private string tenDangNhap = ""; //UserName
        private string matKhau; //Pass
        private int idHang=0; //idCompany
        private int idModel=0; //idModel
        private int soCamera; //AmountCam

        DataTable dtThietBi;
        DataTable dtHang;
        DataTable dtModel;

        public FrmThemThietBi()
        {
            InitializeComponent();
            GetLoaiThietBi();
            cboLoaiThietBi.SelectedValue = 2;
            this.AcceptButton = btnThem;
            getThongTinHangList();
            getThongTinModel(0);
        }

        private void getThongTinModel(int _id)
        {
            string strModel = "select IdModel, ModelName from tblCompanyModel where IdCompany=@idCompany";
            SqlCommand cmdModel = new SqlCommand(strModel, KetNoiCSDL.connection);
            cmdModel.Parameters.Add("@idCompany", SqlDbType.Int).Value = _id;
            SqlDataAdapter daModel = new SqlDataAdapter(cmdModel);
            dtModel = new DataTable();
            daModel.Fill(dtModel);
            cboModel.DataSource = dtModel;
            cboModel.DisplayMember = "ModelName";
            cboModel.ValueMember = "IdModel";

        }

        private void getThongTinHangList()
        {
            string strHang = "select * from tblCompany";
            SqlDataAdapter daHang = new SqlDataAdapter(strHang, KetNoiCSDL.connection);
            dtHang = new DataTable();
            daHang.Fill(dtHang);
            cboHang.DataSource = dtHang;
            cboHang.ValueMember = "IdCompany";
            cboHang.DisplayMember = "CompanyName";
        }

        private void GetLoaiThietBi()
        {
            dtThietBi = new DataTable();
            string strthietBi = "Select IdDeviceType,DeviceType from tblDeviceType";
            SqlDataAdapter daThietBi = new SqlDataAdapter(strthietBi, KetNoiCSDL.connection);
            daThietBi.Fill(dtThietBi);
            cboLoaiThietBi.DataSource = dtThietBi;
            cboLoaiThietBi.ValueMember = "IdDeviceType";
            cboLoaiThietBi.DisplayMember = "DeviceType";
        }

        private void btnHuyBo_Click(object sender, EventArgs e)
        {

            DongForm();

        }

        private void DongForm()
        {
            this.Close();
            thread = new Thread(HienThiThietBi);
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        private void HienThiThietBi()
        {
            Application.Run(new FrmThietBi());
        }

        private void cboLoaiThietBi_SelectedValueChanged(object sender, EventArgs e)
        {
            ComboBox cb = sender as ComboBox;

            try
            {
                idDeviceType = int.Parse(cb.SelectedValue.ToString());
                deviceTypeName = cb.Text;
                if (deviceTypeName.ToLower().Contains("ip"))
                {
                    cboHang.Enabled = true;
                    cboModel.Enabled = true;
                    txtSoCam.ReadOnly = true;
                    txtSoCam.Text = "1";
                }
                else if(deviceTypeName.ToLower().Contains("onvif"))
                {
                    cboHang.Enabled = false;
                    cboModel.Enabled = false;
                    txtSoCam.ReadOnly = true;
                    txtSoCam.Text = "1";
                }
                else
                {
                    cboHang.Enabled = false;
                    cboModel.Enabled = false;
                    txtSoCam.ReadOnly = false;
                    txtSoCam.Text = "1";
                }
            }
            catch (Exception)
            {
                deviceTypeName = "";
            }

        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            getThongTin();

            if (deviceTypeName.ToUpper().Contains("IP"))
            {
               if(KiemTraNhap()==false) 
                { 
                    return; 
                }
                else
                {
                    ThemThietBi(1);
                }                
            }
            else if (deviceTypeName.ToUpper().Contains("ONVIF"))
            {
                if (KiemTraNhap() == false)
                {
                    return;
                }
                else
                {
                    ThemThietBi(2);
                }
                
            }
            else
            {
                if (KiemTraNhap() == false)
                {
                    return;
                }
                else
                {
                    ThemThietBi(3);
                }
            }
           

            MessageBox.Show("Đã thêm mới thiết bị thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            DongForm();
        }

        private void getThongTin()
        {
            tenThietBi = txtTen.Text.Trim();
            diaChiIP = txtDiaChiIP.Text;
            congKetNoi = txtCong.Text;
            tenDangNhap = txtTenDangNhap.Text;
            matKhau = txtMatKhau.Text;
            if (txtSoCam.Text.ToString().Length > 0)
            {
                soCamera = int.Parse(txtSoCam.Text.ToString());
            }
            
        }
       
        private bool KiemTraNhap()
        {
            string thongBao = "";
            bool isPass = true;
            if (tenThietBi.Length == 0)
            {
                thongBao = "Tên không được để trống";
                txtTen.Focus();
                isPass = false;
            }
            else if (tenThietBi.Length < 4)
            {
                thongBao = "Độ dài tên nhiều hơn 4 ký tự";
                txtTen.Focus();
                isPass = false;
            }
            else if (tenThietBi.Length == 0)
            {
                thongBao = "Tên không được để trống";
                txtTen.Focus();
                isPass = false;
            }
            else if (diaChiIP.Length == 0)
            {
                thongBao = "Địa chỉ IP không được trống";
                txtDiaChiIP.Focus();
                isPass = false;
            }
            else if (congKetNoi.ToString().Length == 0)
            {
                thongBao = "Cổng kết nôi không được trống";
                txtCong.Focus();
                isPass = false;
            }
            else if (congKetNoi.ToString().Length < 3 || congKetNoi.ToString().Length > 5)
            {
                thongBao = "Cổng kết nôi tối thiểu 3 ký tự, tối đa 5";
                txtCong.Focus();
                isPass = false;
            }

            else if (tenDangNhap.Length < 4 || tenDangNhap.Length > 20)
            {
                thongBao = "Tên đăng nhập không được để trống. (4-20) ký tự";
                txtTenDangNhap.Focus();
                isPass = false;
            }

            else if (matKhau.Length == 0)
            {
                thongBao = "Mật khẩu không được để trống. Chứa ít nhất 8 ký tự";
                txtMatKhau.Focus();
                isPass = false;
            }
            else if (matKhau.Length < 8)
            {
                thongBao = "Mật khẩu ít nhất 8 ký tự";
                txtMatKhau.Focus();
                isPass = false;
            }

            if (isPass == false)
            {
                MessageBox.Show(thongBao, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return isPass;
        }
       

        private void ThemThietBi(int loaiThietBi)
        {
            string sqlInsert = "insert into TblDevice(IdDeviceType,DeviceName,IpAddress,PortNo,UserName,Pass,idCompany,idModel,AmountCam)";
            sqlInsert += "values(@IdDeviceType,@DeviceName,@IpAddress,@PortNo,@UserName,@Pass,@idCompany,@idModel,@AmountCam)";
            SqlCommand cmdInsert = new SqlCommand();
            cmdInsert.CommandText = sqlInsert;
            cmdInsert.CommandType = CommandType.Text;
            cmdInsert.Parameters.Add("@IdDeviceType", SqlDbType.Int).Value = idDeviceType;
            cmdInsert.Parameters.Add("@DeviceName", SqlDbType.NVarChar).Value = tenThietBi;
            cmdInsert.Parameters.Add("@IpAddress", SqlDbType.NVarChar).Value = diaChiIP;
            cmdInsert.Parameters.Add("@PortNo", SqlDbType.Int).Value = int.Parse(congKetNoi);
            cmdInsert.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = tenDangNhap;
            cmdInsert.Parameters.Add("@Pass", SqlDbType.NVarChar).Value = matKhau;
            cmdInsert.Parameters.Add("@idCompany", SqlDbType.Int).Value = idHang;
            cmdInsert.Parameters.Add("@idModel", SqlDbType.Int).Value = idModel;
            
            if (loaiThietBi==1 || loaiThietBi== 2)
            {
                soCamera = 1;
            }
            cmdInsert.Parameters.Add("@AmountCam", SqlDbType.Int).Value = soCamera;
            cmdInsert.Connection = KetNoiCSDL.connection;
            cmdInsert.ExecuteNonQuery();
        }

        private void txtCong_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) )
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }

        }

        private void txtDiaChiIP_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void txtSoCam_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void cboLoaiThietBi_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cboHang_SelectedValueChanged(object sender, EventArgs e)
        {
            ComboBox cb = sender as ComboBox;
            try
            {
                int id = int.Parse(cboHang.SelectedValue.ToString());
                getThongTinModel(id);
            }
            catch (Exception ex)
            {
             //   MessageBox.Show(ex.Message);
                
            }
        }
    }
}
