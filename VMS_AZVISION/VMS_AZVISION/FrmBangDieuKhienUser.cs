﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VMS_AZVISION
{
    public partial class FrmBangDieuKhienUser : Form
    {
        public FrmBangDieuKhienUser()
        {
            InitializeComponent();
            grdBangDieuKhien.Rows.Add("Thay đổi tài khoản");
            grdBangDieuKhien.Rows.Add("Hướng dẫn sử dụng");
            grdBangDieuKhien.Rows.Add("Thông tin phần mềm");
            grdBangDieuKhien.AllowUserToAddRows = false;
            grdBangDieuKhien.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            grdBangDieuKhien.AllowUserToResizeColumns = false;
            grdBangDieuKhien.ColumnHeadersHeightSizeMode =
        DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            grdBangDieuKhien.AllowUserToResizeRows = false;
            grdBangDieuKhien.RowHeadersWidthSizeMode =
                DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            
        }

        private void grdBangDieuKhien_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string dong = grdBangDieuKhien.Rows[e.RowIndex].Cells[0].Value.ToString();
            MessageBox.Show("Đã chọn chức năng" + dong,"Thông báo", MessageBoxButtons.OK,MessageBoxIcon.Information); 

            this.Close();
        }
    }
}
