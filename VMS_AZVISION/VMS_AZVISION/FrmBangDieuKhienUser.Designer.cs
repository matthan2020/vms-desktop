﻿namespace VMS_AZVISION
{
    partial class FrmBangDieuKhienUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grdBangDieuKhien = new System.Windows.Forms.DataGridView();
            this.colNoiDung = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.grdBangDieuKhien)).BeginInit();
            this.SuspendLayout();
            // 
            // grdBangDieuKhien
            // 
            this.grdBangDieuKhien.BackgroundColor = System.Drawing.Color.Black;
            this.grdBangDieuKhien.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdBangDieuKhien.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdBangDieuKhien.ColumnHeadersVisible = false;
            this.grdBangDieuKhien.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colNoiDung});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdBangDieuKhien.DefaultCellStyle = dataGridViewCellStyle2;
            this.grdBangDieuKhien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdBangDieuKhien.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.grdBangDieuKhien.Location = new System.Drawing.Point(0, 0);
            this.grdBangDieuKhien.Name = "grdBangDieuKhien";
            this.grdBangDieuKhien.ReadOnly = true;
            this.grdBangDieuKhien.RowHeadersVisible = false;
            this.grdBangDieuKhien.Size = new System.Drawing.Size(204, 78);
            this.grdBangDieuKhien.TabIndex = 6;
            this.grdBangDieuKhien.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdBangDieuKhien_CellContentClick);
            // 
            // colNoiDung
            // 
            this.colNoiDung.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(176)))), ((int)(((byte)(255)))));
            this.colNoiDung.DefaultCellStyle = dataGridViewCellStyle1;
            this.colNoiDung.FillWeight = 150F;
            this.colNoiDung.HeaderText = "NoiDung";
            this.colNoiDung.Name = "colNoiDung";
            this.colNoiDung.ReadOnly = true;
            // 
            // FrmBangDieuKhienUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(204, 78);
            this.Controls.Add(this.grdBangDieuKhien);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmBangDieuKhienUser";
            this.Text = "FrmBangDieuKhienUser";
            ((System.ComponentModel.ISupportInitialize)(this.grdBangDieuKhien)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView grdBangDieuKhien;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNoiDung;
    }
}