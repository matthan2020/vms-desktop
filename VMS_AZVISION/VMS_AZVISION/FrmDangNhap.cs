﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Data.SqlClient;
using VMS_AZVISION.Model;

namespace VMS_AZVISION
{
    public partial class FrmDangNhap : Form
    {
        int soLan = 0;
        Thread th;
       // SqlConnection connection;
        public FrmDangNhap()
        {
            InitializeComponent();
            SetBgr();
            KetNoiCSDL.KetNoi();
            SetForm();
            this.AcceptButton = btnDangNhap;
        }
        private void ClearForm()
        {
            txtTenDangNhap.Clear();
            txtMatKhau.Clear();
            txtTenDangNhap.Focus();
        }
        private void SetForm()
        {
            txtTenDangNhap.Text = "admin1234";
            txtMatKhau.Text = "admin1234";
        }
        private void SetBgr()
        {
            panelLeftIcon.BackColor = Color.FromArgb(0, Color.Black);
            panelTopLeft.BackColor = Color.FromArgb(0, Color.Black);
            panelLeft.BackColor = Color.FromArgb(0, Color.Black);
            groupBox1.BackColor = Color.FromArgb(0, Color.Black);
            panel1.BackColor = Color.FromArgb(0, Color.Black);
            panel2.BackColor = Color.FromArgb(0, Color.Black);
            panel3.BackColor = Color.FromArgb(0, Color.Black);
        }

        //private void KetNoi()
        //{
        //    try
        //    {
        //        string strConn = "Persist Security Info = False; Data Source = QUANGHUYIT\\SQLSERVER0703; Initial Catalog = VMS; User Id = sa; Password = !@ADMINITCKLK";
        //        connection = new SqlConnection();
        //        connection.ConnectionString = strConn;
        //        connection.Open();
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }
            
        //}

        
        private void btnThoat_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnDangNhap_Click(object sender, EventArgs e)
        {
            string matKhau = txtMatKhau.Text.ToString().Replace(" ", "");
            string taiKhoan = txtTenDangNhap.Text.ToString().Replace(" ", "");
            string getMatKhau = "";
            string getTaiKhoan = "";
            
            soLan++;
            SqlDataAdapter da= new SqlDataAdapter("select pwd, UserName from tblUser", KetNoiCSDL.connection);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if(dt.Rows.Count>0)
            {
                getMatKhau = dt.Rows[0].ItemArray[0].ToString();
                getTaiKhoan = dt.Rows[0].ItemArray[1].ToString();
            }

            if (matKhau.Length ==0 || taiKhoan.Length == 0)
            {
                lblThongBao.Text = "Vui lòng nhập tên đăng nhập và mật khẩu";
                txtTenDangNhap.Focus();
                lblThongBao.Visible = true;
                return;
            }
            else if(taiKhoan.Length < 8 || taiKhoan.Length > 20)
            {
                lblThongBao.Text = "Tên đăng nhập tối thiểu là 8 ký tự và tối đa 20 ký tự \n Không có khoảng trắng";
                lblThongBao.Visible = true;
                txtTenDangNhap.Focus();
                return;
            }
            else if (matKhau.Length < 8 || matKhau.Length > 16)
            {
                lblThongBao.Text = "Mật khẩu tối thiểu là 8 ký tự và tối đa 16 ký tự \n Không có khoảng trắng";
                txtMatKhau.Focus();
                lblThongBao.Visible = true;
                return;

            }
            else if(matKhau.Equals(getMatKhau) && taiKhoan.Equals(getTaiKhoan))
            {
                this.Close();
                th = new Thread(HienThiTrangChu);
                th.SetApartmentState(ApartmentState.STA);
                th.Start();
            }
            else if(soLan>=5)
            {
                MessageBox.Show("Tài khoản bạn đã nhập quá số lần quy định. Vui lòng liên hệ quản trị viên để được hỗ trợ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtTenDangNhap.Clear();
                txtMatKhau.Clear();
                txtTenDangNhap.Enabled = false;
                txtMatKhau.Enabled = false;
                btnDangNhap.Enabled = false;
                lblThongBao.Visible = false;
                return;
            }
            else
            {
                lblThongBao.Text = "Sai tên đăng nhập hoặc mật khẩu ";
                lblThongBao.Visible = true;
                txtTenDangNhap.Focus();
                return;
            }

        }

        private void HienThiTrangChu()
        {
            Application.Run(new FrmTrangChu());
        }

        private void chkGhiNhoMatKhau_Click(object sender, EventArgs e)
        {

        }
    }
}
