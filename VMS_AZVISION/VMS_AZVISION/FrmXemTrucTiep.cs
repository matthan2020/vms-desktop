﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VMS_AZVISION
{
    public partial class FrmXemTrucTiep : Form
    {
        public FrmXemTrucTiep()
        {
            InitializeComponent();
            HienThiDongHo();
        }
        private void HienThiDongHo()
        {
            timer1.Enabled = true;
            lblDongHo.Text = DateTime.Now.ToString();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblDongHo.Text = DateTime.Now.ToString();
        }
    }
}
