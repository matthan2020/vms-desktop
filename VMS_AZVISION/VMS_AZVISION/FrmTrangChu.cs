﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace VMS_AZVISION
{
    public partial class FrmTrangChu : Form
    {
        private bool hienThiBangDieuKhien;
        public FrmTrangChu()
        {
            InitializeComponent();
            hienThiBangDieuKhien = false;
            lblDongHo.Text = DateTime.Now.ToString("hh:mm:ss");
        }



        private void OpenNewForm()
        {
            Application.Run(new FrmThemThietBi());
        }

        private void btnDongForm_Click(object sender, EventArgs e)
        {
            DialogResult dlg = MessageBox.Show("Bạn có chắc chắn muốn thoát chương trình", "Thoát chương trình", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dlg == DialogResult.Yes)
            {
                Application.Exit();
            }
            else
            {
                return;
            }

        }

        private void btnXemTrucTiep_Click(object sender, EventArgs e)
        {
            FrmXemTrucTiep f = new FrmXemTrucTiep();
            f.ShowDialog();
        }

        private void btnSuKien_Click(object sender, EventArgs e)
        {
            FrmDanhSachSuKien f = new FrmDanhSachSuKien();
            f.ShowDialog();
        }

        private void btnPhatLai_Click(object sender, EventArgs e)
        {
            FrmXemPhatLai f = new FrmXemPhatLai();
            f.ShowDialog();
        }

        private void btnNguoiDung_Click(object sender, EventArgs e)
        {
            FrmQuanLyTaiKhoan f = new FrmQuanLyTaiKhoan();
            f.ShowDialog();
        }

        private void btnCauHinhHeThong_Click(object sender, EventArgs e)
        {
            FrmCauHinhHeThong f = new FrmCauHinhHeThong();
            f.ShowDialog();
        }

        private void btnThietBi_Click(object sender, EventArgs e)
        {
            FrmThietBi f = new FrmThietBi();
            f.ShowDialog();
        }

        private void picMaxRestore_Click(object sender, EventArgs e)
        {

        }

        private void picClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void picUser_Click(object sender, EventArgs e)
        {
            hienThiBangDieuKhien = !hienThiBangDieuKhien;
            int _top = panel8.Height;
            int _left = this.Width-panel8.Width;

            FrmBangDieuKhienUser f = new FrmBangDieuKhienUser();
            f.StartPosition = FormStartPosition.Manual;
            f.Left = _left;
            f.Top = _top;
            if (hienThiBangDieuKhien)
            {
                f.Show();
            }
            else
            {
                FormCollection fc = Application.OpenForms;
                foreach (Form _item in fc)
                {
                    if (_item.Name.Equals("FrmBangDieuKhienUser"))
                    {
                        _item.Hide();
                    }
                }
            }
           
        }

        private void picConfig_Click(object sender, EventArgs e)
        {
            hienThiBangDieuKhien = !hienThiBangDieuKhien;
            int _top = panel8.Height;
            int _left = this.Width - panel8.Width;

            FrmBangDieuKhienUser f = new FrmBangDieuKhienUser();
            f.StartPosition = FormStartPosition.Manual;
            f.Left = _left;
            f.Top = _top;
            if (hienThiBangDieuKhien)
            {
                f.Show();
            }
            else
            {
                FormCollection fc = Application.OpenForms;
                foreach(Form _item in fc)
                {
                    if (_item.Name.Equals("FrmBangDieuKhienUser"))
                    {
                        _item.Hide();
                    }
                }
                
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblDongHo.Text = DateTime.Now.ToString("hh:mm:ss");
        }
    }
}
