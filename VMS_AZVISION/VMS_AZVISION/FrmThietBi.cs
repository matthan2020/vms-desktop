﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Data.SqlClient;
using VMS_AZVISION.Model;

namespace VMS_AZVISION
{
    public partial class FrmThietBi : Form
    {
        Thread th;
        private bool hienThiBangDieuKhien;
        private int tatCaThietBi=0;
        private int thietBiOnline = 0;
        private int thietBiKhac = 0;

        public FrmThietBi()
        {
            InitializeComponent();
            lblDongHo.Text = DateTime.Now.ToString("hh:mm:ss");
            grdThietBi.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(61, 61, 61);
            grdThietBi.EnableHeadersVisualStyles = false;
            grdThietBi.AutoGenerateColumns = false;
            grdThietBi.AllowUserToAddRows = false;
            HienThiDanhSachThietBi();
        }

        private void HienThiDanhSachThietBi()
        {
            DataTable dt = new DataTable();
            string strThietBi = "procDanhSachThietBi";
            SqlCommand cmdThietBi = new SqlCommand(strThietBi, KetNoiCSDL.connection);
            cmdThietBi.CommandType = CommandType.StoredProcedure;
            
            SqlDataAdapter daThietBi = new SqlDataAdapter(cmdThietBi);
            daThietBi.Fill(dt);
            grdThietBi.DataSource = dt;
            tatCaThietBi = dt.Rows.Count;

            SqlCommand cmdTbiOnline = new SqlCommand("select count(DeviceId) DeviceNo from tbldevice t where t.IdStatus=2", KetNoiCSDL.connection);
            cmdTbiOnline.CommandType = CommandType.Text;
            DataTable dtTbOnline = new DataTable();
            SqlDataAdapter daTbiOnline = new SqlDataAdapter(cmdTbiOnline);
            daTbiOnline.Fill(dtTbOnline);

            if (dtTbOnline.Rows.Count>0)
            {
                thietBiOnline = int.Parse(dtTbOnline.Rows[0].ItemArray[0].ToString());
            }
            tabHienThiSoLuong();
        }
        
        private void tabHienThiSoLuong()
        {
            lblTatCaThietBi.Text = "Tất cả thiết bị (" + tatCaThietBi.ToString() + ")";
            lblThietBiOnline.Text = "Thiết bị online (" + thietBiOnline.ToString() + ")";
            lblThietBiKhac.Text = "Thiết bị khác (" + thietBiKhac.ToString() + ")";

        }

        private void HienThiTrangChu()
        {
            Application.Run(new FrmTrangChu());
        }

        private void HienThiThemThietBi()
        {
            Application.Run(new FrmThemThietBi());
        }

        private void picClose_Click(object sender, EventArgs e)
        {
            this.Close();
            th = new Thread(HienThiTrangChu);
            th.SetApartmentState(ApartmentState.STA);
            th.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblDongHo.Text = DateTime.Now.ToString("hh:mm:ss");
        }

        private void picUser_Click(object sender, EventArgs e)
        {
            hienThiBangDieuKhien = !hienThiBangDieuKhien;
            int _top = panel8.Height;
            int _left = this.Width - panel8.Width;

            FrmBangDieuKhienUser f = new FrmBangDieuKhienUser();
            f.StartPosition = FormStartPosition.Manual;
            f.Left = _left;
            f.Top = _top;
            if (hienThiBangDieuKhien)
            {
                f.Show();
            }
            else
            {
                FormCollection fc = Application.OpenForms;
                foreach (Form _item in fc)
                {
                    if (_item.Name.Equals("FrmBangDieuKhienUser"))
                    {
                        _item.Hide();
                    }
                }
            }
        }

        private void picConfig_Click(object sender, EventArgs e)
        {
            hienThiBangDieuKhien = !hienThiBangDieuKhien;
            int _top = panel8.Height;
            int _left = this.Width - panel8.Width;

            FrmBangDieuKhienUser f = new FrmBangDieuKhienUser();
            f.StartPosition = FormStartPosition.Manual;
            f.Left = _left;
            f.Top = _top;
            if (hienThiBangDieuKhien)
            {
                f.Show();
            }
            else
            {
                FormCollection fc = Application.OpenForms;
                foreach (Form _item in fc)
                {
                    if (_item.Name.Equals("FrmBangDieuKhienUser"))
                    {
                        _item.Hide();
                    }
                }

            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            this.Close();
            th = new Thread(HienThiThemThietBi);
            th.SetApartmentState(ApartmentState.STA);
            th.Start();
        }

        private void grdThietBi_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string content = grdThietBi.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
            int deviceId = int.Parse(grdThietBi.Rows[e.RowIndex].Cells["ColDeviceId"].Value.ToString());
            string tenThietBi = grdThietBi.Rows[e.RowIndex].Cells["colTen"].Value.ToString();
            if (content.ToLower().Contains("xóa"))
            {
                DialogResult dlg = MessageBox.Show("Bạn đồng ý xóa thiết bị này " + tenThietBi, "Xóa thiết bị", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlg == DialogResult.Yes)
                {
                    string strXoa = "delete from TblDevice where DeviceId=@DeviceId";
                    SqlCommand cmdXoaThietBi = new SqlCommand(strXoa, KetNoiCSDL.connection);
                    cmdXoaThietBi.Parameters.Add("@DeviceId", SqlDbType.Int).Value = deviceId;
                    cmdXoaThietBi.ExecuteNonQuery();
                    HienThiDanhSachThietBi();
                }
            }
            else if ((content.ToLower().Contains("điều chỉnh")))
            {
                FrmThemThietBi f = new FrmThemThietBi();
                f.ShowDialog();
            }
        }
    }
}
